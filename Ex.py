from tkinter import *
from tkinter.ttk import Radiobutton


def clicked():
    if selected.get() == 1 and selected1.get() == 4 or selected.get() == 2 and selected1.get() == 4:
        lbl.configure(text="Ты обычный гном")
    elif selected.get() == 1 and selected1.get() == 5 or selected.get() == 2 and selected1.get() == 5:
        lbl.configure(text="Ты гигагном")
    elif selected.get() == 1 and selected1.get() == 6 or selected.get() == 2 and selected1.get() == 6:
        lbl.configure(text="Ты прораб")
    elif selected.get() == 3 and selected1.get() == 5:
        lbl.configure(text="Ты Антоновка")
    elif selected.get() == 3 and selected1.get() == 4:
        lbl.configure(text="Ты Антон Николаевич")
    elif selected.get() == 3 and selected1.get() == 6:
        lbl.configure(text="Ты не гном")
    else:
        lbl.configure(text="Допройди тест")


window = Tk()
window.title("Какой ты гном?")
window.geometry('400x200')
selected = IntVar()
selected1 = IntVar()
lbl = Label(window)
lbl2 = Label(window, text="Твое любимое занятие?")
lbl3 = Label(window, text="Твой рост?")
rad1 = Radiobutton(window, text='Копать', value=1, variable=selected)
rad2 = Radiobutton(window, text='Чесать бороду', value=2, variable=selected)
rad3 = Radiobutton(window, text='Использовать Linux', value=3, variable=selected)
rad4 = Radiobutton(window, text='Метр', value=4, variable=selected1)
rad5 = Radiobutton(window, text='1.5 метра', value=5, variable=selected1)
rad6 = Radiobutton(window, text='2 метра', value=6, variable=selected1)
btn = Button(window, text="Клик", command=clicked)
rad1.grid(column=0, row=1)
rad2.grid(column=1, row=1)
rad3.grid(column=2, row=1)
rad4.grid(column=0, row=3)
rad5.grid(column=1, row=3)
rad6.grid(column=2, row=3)
btn.grid(column=1, row=4)
lbl.grid(column=1, row=5)
lbl2.grid(column=1, row=0)
lbl3.grid(column=1, row=2)

window.mainloop()